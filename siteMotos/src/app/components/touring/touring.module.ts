import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TouringComponent } from './touring.component';



@NgModule({
  declarations: [
    TouringComponent
  ],
  imports: [
    CommonModule
  ]
})
export class TouringModule { }
