import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StreetComponent } from './street.component';



@NgModule({
  declarations: [
    StreetComponent
  ],
  imports: [
    CommonModule
  ]
})
export class StreetModule { }
