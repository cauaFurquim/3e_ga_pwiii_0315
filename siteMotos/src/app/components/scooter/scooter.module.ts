import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScooterComponent } from './scooter.component';



@NgModule({
  declarations: [
    ScooterComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ScooterModule { }
