import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CadastroMotosComponent } from './cadastro-motos.component';



@NgModule({
  declarations: [
    CadastroMotosComponent
  ],
  imports: [
    CommonModule
  ]
})
export class CadastroMotosModule { }
