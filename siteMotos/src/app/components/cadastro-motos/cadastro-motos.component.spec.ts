import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastroMotosComponent } from './cadastro-motos.component';

describe('CadastroMotosComponent', () => {
  let component: CadastroMotosComponent;
  let fixture: ComponentFixture<CadastroMotosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CadastroMotosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastroMotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
